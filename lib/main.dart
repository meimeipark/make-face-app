import 'package:flutter/material.dart';
import 'package:make_face_app2/pages/page_index.dart';
import 'package:make_face_app2/pages/page_make_face.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
        fontFamily: 'Aggro',
      ),
      home: PageIndex(
      ),
    );
  }
}
