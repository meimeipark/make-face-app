import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:make_face_app2/pages/page_make_face.dart';


class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _formKey = GlobalKey<FormBuilderState>();
//  TextEditingController tec = TextEditingController();

  int clothesNumber = 1;
  int eyebrowNumber = 1;
  int eyesNumber = 1;
  int hairNumber = 1;
  int mouseNumber = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('친구 얼굴 만들기'),
        centerTitle: true,
        backgroundColor: Colors.blue[300],
        elevation: 0,
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text('User',
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w300,
                color: Colors.grey[800],
                ),
              ),
              accountEmail: Text('user1234@gmail.com',
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 13,
                color: Colors.grey[800],
              ),),
              currentAccountPicture: Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: CircleAvatar(
                  backgroundColor: Colors.blue[300],
                  backgroundImage: AssetImage(
                    'assets/body.png'
                  ),
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.blue[100],
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0)
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.home, color: Colors.grey[600],
              ),
              title: Text('Home'),
            ),
            ListTile(
              leading: Icon(Icons.people, color: Colors.grey[600],
              ),
              title: Text('Friends'),
            ),
            ListTile(
              leading: Icon(Icons.settings, color: Colors.grey[600],
              ),
              title: Text('Setting'),
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Text(
                'RANDOM',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 28,
                  color: Colors.blue[300],
                ),
              ),
            ),
            Container(
              child: Text(
                'FRIENDS',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.w700,
                  color: Colors.blue[700],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 50),
              child: Text(
                'FACE MAKER',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 28,
                  color: Colors.blue[300],
                ),
              ),
            ),
            Container(
                width: 300,
                height: 60,
                margin: EdgeInsets.fromLTRB(0, 0, 0, 50),
              child: FormBuilder(
                  key: _formKey,
                  child: Column(
                    children: [
                    Container(
                      width: 300,
                      height: 60,
                      child: FormBuilderTextField(
                        name: 'friendName',
                        decoration: const InputDecoration(
                          labelText: '친구이름',
                          border: OutlineInputBorder(),
                        ),
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(),
                        ]),
                      ),
                    ),
                ]),
              ),
            ),
            ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue[300],
                    onPrimary: Colors.white,
                    elevation: 0.0,
                    minimumSize: const Size(250, 70),
                  ),
              onPressed: () {
                // 만약에 폼에 값이 있으면 saveAndValidate, 값이 없으면 false가 리턴되어서 if문이 실행되지 않음
                if (_formKey.currentState?.saveAndValidate() ?? false){
                  String friendName = _formKey.currentState!.fields['friendName']!.value;
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMakeFace(friendName: friendName)));
                }
              },
              child: const Text(
                '시작',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  ),
              ),
            ),
          ]),
      ),
    );
  }
}
