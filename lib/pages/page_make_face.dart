import 'package:flutter/material.dart';
import 'package:make_face_app2/pages/page_index.dart';
import 'dart:math';


class PageMakeFace extends StatefulWidget {
  const PageMakeFace({
    super.key,
    required this.friendName,
  });

  final String friendName;

  @override
  State<PageMakeFace> createState() => _PageMakeFaceState();
}

class _PageMakeFaceState extends State<PageMakeFace> {
  int mouseNumber = Random().nextInt(5) + 1;
  int clothesNumber = Random().nextInt(5) + 1;
  int eyebrowNumber = Random().nextInt(5) + 1;
  int eyesNumber = Random().nextInt(5) + 1;
  int hairNumber = Random().nextInt(5) + 1;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('매칭 결과'),
        centerTitle: true,
        backgroundColor: Colors.blue[200],
        elevation: 0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: Text(
                widget.friendName,
                style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 1.2,
                  fontWeight: FontWeight.w400,
                  color: Colors.blue,
                ),
              ),
            ),
            Stack(
              children: [
                Container(
                  width: 400,
                  height: 400,
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/body.png',
                        width: 300,
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                  ),
                Container(
                  width: 400,
                  height: 400,
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/clothes/clothes$clothesNumber.png',
                        width: 300,
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 400,
                  height: 400,
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/eyebrow/eyebrow$eyebrowNumber.png',
                        width: 300,
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 400,
                  height: 400,
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/eyes/eyes$eyesNumber.png',
                        width: 300,
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 400,
                  height: 400,
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/hair/hair$hairNumber.png',
                        width: 300,
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 400,
                  height: 400,
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/mouse/mouse$mouseNumber.png',
                        width: 300,
                        height: 300,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue[300],
                  elevation: 0,
                  minimumSize: const Size(300, 60),
                ),
                onPressed: () {
                  Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PageIndex()),);
                },
                child: const Text(
                  '종료',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
